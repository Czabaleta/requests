import requests
import json

photos = []
users = []
todos = []
comments = []

p = requests.get('https://jsonplaceholder.typicode.com/photos')
u = requests.get('https://jsonplaceholder.typicode.com/users')
t = requests.get('https://jsonplaceholder.typicode.com/todos')
c = requests.get('https://jsonplaceholder.typicode.com/comments')

photos = json.loads(p.text)
users = json.loads(u.text)
todos = json.loads(t.text)
comments = json.loads(c.text)

html = open('LSV-TEAM.html', 'w')

mensaje = """<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>LSV-TEAM</title>
    <style>
        p.padding {
          padding: 15px 40px;
        }
        p.border{
          border-bottom: 1px solid;
        }
        strong.color{
          color: #ffffff;
        }
    </style>
</head>
<body>
    <div class="container">
        <br/><br/>
        <h1 class="text-center">LSV-TEAM</h1>
        <br/><br/>
        <h4>Photos</h4>
        <div class="row">"""

mensaje += ''
for i in photos[0:4]:
    mensaje += '''
    <div class="col" >
              <img src="''' + i['url']+'''" alt="''' + i['title']+'''" class="img-thumbnail">
            </div>
    '''
mensaje += '''
        </div>
        <br/><br/>
        <h4>Users</h4>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Usuario</th>
              <th scope="col">E-mail</th>
            </tr>
          </thead>
          <tbody>
            
'''
for i in users[0:10]:

    mensaje += '''
              <tr>
              <th scope="row">''' + str(i['id'])+'''</th>
              <td>''' + i['name']+'''</td>
              <td>''' + i['username']+'''</td>
              <td>''' + i['email']+'''</td>
            </tr>'''

mensaje += '''
           </tbody>
        </table>
        <br/><br/>
        <h4>To Do</h4>
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Título</th>
              <th scope="col">Completado</th>
            </tr>
          </thead>
          <tbody>'''

for i in todos[0:10]:
    if i['completed']:
        i['completed'] = 'Si'
    else:
        i['completed'] = 'No'
    mensaje += '''
    
              <tr>
              <th scope="row">''' + str(i['id'])+'''</th>
              <td>''' + i['title']+'''</td>
              <td>''' + i['completed'] + '''</td>
            </tr>'''
mensaje += '''
          </tbody>
        </table>
        <br/><br/>
        <h3>Comentarios</h3>
        <div class="row">'''

for i in comments[0:4]:
    mensaje += '''
        
            <div class="col-6">
              <p class="bg-dark padding"><strong class="color">''' + str(i['id'])+'''</strong></p>
              <br/>
              <p class="border">''' + i['name']+'''</p>
              <br/>
              <p class="border">''' + i['email']+'''</p>
              <br/>
              <p class="bd-callout-info">''' + i['body']+'''</p>
              <br/>
            </div>'''
mensaje += '''
        </div>
    </div>
</body>
</html>'''

html.write(mensaje)

html.close()
